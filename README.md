# spearace-exam

I use laravel as my framework
For user auth in the endpoint I laravel passport

How to install
- edit the env to specifice db settings
ON CMD
- run install composer
-run php artisan migrate
- run php artisan db:seed
- run php artisan passport:install --force
- copy client id 2 for client secret
- run php artisan serve



ROUTES
-For user registration
TYPE|POST
/register
Field:
name|text
email|text
password|text

-To request a token 
TYPE|POST
/outh/token
Field:
grant_type|text: password
client_id|text: 2
client_secret|text: refer to passport install

-USER route
Must have authorization header and token 
TYPE|GET
/user/details
TYPE|POST(admin account only)
/user/store
Field
name: text
email: text
password: text

TYPE|PUT
/user/update(USER)
/user/admin/update/{id}(ADMIN)

TYPE(DELETE)
/user/admin/delete/{id}(ADMIN)

-CONTACT Route(must be login) 
TYPE|GET
/contacts

TYPE|POST
/contacts/store
Field
name:text
phone:min10|max10

TYPE|DELETE
/contacts/delete/{id}

I havent created any unit test because of lack of time I apologize