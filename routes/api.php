<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', 'UserController@store');

Route::middleware(['auth:api'])->group(function () {

	// USER
	Route::get('/user/details', 'UserController@index');
	Route::post('/user/store', 'UserController@store_admin');
	Route::delete('/user/delete/{id}', 'UserController@destroy');
	Route::put('/user/update', 'UserController@user_update');
	Route::put('/user/admin/update/{id}', 'UserController@admin_update');

	// CONTACTS
	Route::get('/contacts', 'ContactController@index');
	Route::post('/contact/store', 'ContactController@store');
	Route::delete('/contact/delete/{id}', 'ContactController@destroy');

});
