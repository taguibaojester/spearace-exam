<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users=[
            [
                'name' => 'Jester Taguibao',
                'email' => 'taguibaojester@gmail.com',
                'password' => bcrypt('secret123'),
                'role' => 'admin',
            ],
            [
                'name' => 'User One',
                'email' => 'user1@gmail.com',
                'password' => bcrypt('secret123'),
                'role' => 'user',
            ]
        ];

        DB::table('users')->insert($users);

    }
}
