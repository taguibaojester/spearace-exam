<?php

namespace App\Http\Controllers;

use App\Contact;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Contact as ContactResource;

class ContactController extends Controller
{

    public function index()
    {
        $id = Auth::id();
        return ContactResource::collection(Contact::where('user_id', $id)->paginate(10));
        
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        $request->validate([
            'name' => 'required|max:50',
            'phone' => 'required|min:10|max:10',
        ]);

        $phone = Contact::where('user_id', $id)->where('phone', '+63'.$request->phone)->first();
        
        if($phone!=null)
        {  
            return array('status'=>'failed', 'message'=>'This number already exist');            
        }        

        $contact = new Contact();
        $contact->user_id = $id;
        $contact->name = $request->name;
        $contact->phone = '+63'.$request->phone;
        $contact->save();

        return array('status'=>'success', 'message'=>'New contact has been created');
    }


    public function show(Contact $contact)
    {
        //
    }

    public function update(Request $request, Contact $contact)
    {
        //
    }

    public function destroy($id)
    {
        $uid = Auth::id();
        $user = User::where('id', $uid)->first();
        $contact = Contact::where('id', $id)->first();
        // dd($user);
        if($contact==null)
        {
            return array('status'=>'failed', 'message'=>'Contact do not exist');
        }
        if($user->role=='admin' || $contact->user_id==$uid)
        {
            $contact->delete();
            return array('status'=>'success', 'message'=>'Contact has been deleted');
        }
        return array('status'=>'failed', 'message'=>'Failed to delete contact');
    }
}
