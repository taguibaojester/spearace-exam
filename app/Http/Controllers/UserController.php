<?php

namespace App\Http\Controllers;

Use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{

    public function index()
    {
        $id = Auth::id();

        return UserResource::collection(User::where('id', $id)->get());
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return array('status'=>'success', 'message'=>'New user has been created');
    }

    public function store_admin(Request $request)
    {

        $role = User::find(Auth::id());
        
        if($role->role=='admin')
        {
            $request->validate([
                'name' => 'required|max:255',
                'email' => 'required',
                'role' => 'required',
                'password' => 'required',
            ]);

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->role = $request->role;
            $user->password = bcrypt($request->password);
            $user->save();

            return array('status'=>'success', 'message'=>'New user has been created');
        }

        return array('status'=>'failed', 'message'=>'You are not an admin');
        
    }

    public function user_update(Request $request)
    {
        $user = User::find(Auth::id());

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
        ]);

        if($request->get('password')!="")
        {
            $user->password=bcrypt($request->get('password'));
        }  

        $user->name = $request->name;
        $user->email = $request->email;                  
        $user->save();

        return array('status'=>'success', 'message'=>'User has been updated');

    }

    public function admin_update(Request $request, $id)
    {
        $role = User::find(Auth::id());
        
        if($role->role=='admin')
        {
            $user = User::find($id);

            $request->validate([
                'name' => 'required|max:255',
                'email' => 'required',
            ]);

            if($request->get('password')!="")
            {
                $user->password=bcrypt($request->get('password'));
            }  

            $user->name = $request->name;
            $user->email = $request->email;                  
            $user->save();

            return array('status'=>'success', 'message'=>'User has been updated');
        }

        return array('status'=>'failed', 'message'=>'You are not an admin');
    }

    public function destroy($id)
    {
        $role = User::find(Auth::id());
        if($role->role=='admin')
        {
            $user = User::find($id);
            if($user==null)
            {
                return array('status'=>'failed', 'message'=>'User does not exist');
            }
        
            $user->delete();
            return array('status'=>'success', 'message'=>'User has been deleted');
        }

        return array('status'=>'failed', 'message'=>'You are not an admin');
    }
}
